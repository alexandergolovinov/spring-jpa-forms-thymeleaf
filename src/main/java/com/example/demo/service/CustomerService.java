package com.example.demo.service;


import com.example.demo.entity.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();
    Customer findCustomerById(Long id);
    void save(Customer customer);

}
